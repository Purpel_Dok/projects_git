// Copyright Epic Games, Inc. All Rights Reserved.

#include "projects_gitGameMode.h"
#include "projects_gitHUD.h"
#include "projects_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aprojects_gitGameMode::Aprojects_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aprojects_gitHUD::StaticClass();
}
