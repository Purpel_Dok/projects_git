// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "projects_gitGameMode.generated.h"

UCLASS(minimalapi)
class Aprojects_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aprojects_gitGameMode();
};



