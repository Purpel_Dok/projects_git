// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class projects_git : ModuleRules
{
	public projects_git(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
